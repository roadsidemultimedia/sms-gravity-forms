<?php
/*
Plugin Name: GForms
Plugin URI: http://www.roadsidemultimedia.com
Description: Allows you to add a hidden gravity form that can be called by any button using #tag and class 
Author: Curtis Grant
PageLines: true
Version: 1.1.1
Section: true
Class Name: GForms
Filter: component ,custom
Loading: active
Text Domain: dms-sms
Bitbucket Plugin URI: https://bitbucket.org/roadsidemultimedia/sms-image-revealer
Bitbucket Branch: master
*/
if( ! class_exists( 'PageLinesSectionFactory' ) )
  return;


class GForms extends PageLinesSection {

  function section_styles(){
    
    wp_enqueue_script( 'magnific-gform', $this->base_url.'/js/jquery.magnific-popup.js', array( 'jquery' ), pl_get_cache_key(), true );
    wp_enqueue_style( 'magnific-gform', $this->base_url.'/css/magnific-popup.css');
    
  }
  function section_persistent(){
    
  }

  function section_opts(){

    $opts = array(
      array(
        'type'    => 'text',
        'key'   => 'gforms_id',
        'label'   => __( 'Form ID', 'pagelines' ),
        'default' => false,
      )
    );

    return $opts;

  }

  /**
  * Section template.
  */
     function section_head() {

    // Global Variables (pl_setting)
    $gformsid = ( $this->opt('gforms_id') ) ? $this->opt('gforms_id') : "";
  ?>
  <script>
  jQuery(function($) {
      // Window Loaded function
      $( window ).ready(function() {
        $('.open-popup-link').magnificPopup({
          type:'inline',
          midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
        });
      });
  });
  </script>

<?php }

   function section_template() {

    // Global Variables (pl_setting)
    $gformsid = ( $this->opt('gforms_id') ) ? $this->opt('gforms_id') : "";
  ?>
  <div class="sms-gforms">GRAVITY FORMS - Href= #gform<?php echo $gformsid; ?> | Class=open-popup-link</div>
  <div id="gform<?php echo $gformsid; ?>" class="white-popup mfp-hide">
      <?php gravity_form($gformsid, true, true, false, '', true); ?>
  </div>

<?php }

}


